## CPrAN Docker container

The current version of the Docker container runs CPrAN 0.0413 and Praat
6.0.29 barren for Linux 64-bit.

You can use this container to run CPrAN locally without installing it on
your system. This will be most useful if you run it interactively.

    docker run -it --rm jjatria/cpran /bin/bash

The default user in the image is called "user", so its preferences directory
will be at `/home/user/.praat-dir`, and that's where the client will make
its changes. To make the changes permanent, run it specifying your host's
preferences directory as the volume for the container's preference directory.

    docker run -it --rm \
      --volume ~/.praat-dir:/home/user/.praat-dir \
      jjatria/cpran /bin/bash

The image also runs with `/home/user/.local/bin` at the start of `$PATH`,
which makes it easy to add executables, or change the version of the
executable that is running.

    docker run -it --rm \
      --volume ~/bin:/home/user/.local/bin \
      jjatria/cpran /bin/bash

Bear in mind that the container will not be able to execute a non-barren
version of Praat.

All these commands make a single-run container. A possibly more convenient
way to use it is to run the container in detached mode.

    docker run -it --name cpran \
      --volume ~/.praat-dir:/home/user/.praat-dir \
      --detach jjatria/cpran /bin/bash

If this command complains of the "cpran" container already existing from a
previous run, you can remove it with `docker rm cpran`.

With that set, `docker exec` can be used to send commands to the container,
which will make changes in the local filesystem as well

    docker exec cpran cpran install vieweach
