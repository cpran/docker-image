FROM ubuntu

MAINTAINER José Joaquín Atria jjatria@gmail.com

RUN CACHE=20161122 apt-get update
RUN apt-get install -y build-essential git unzip curl \
  perl liblwp-protocol-https-perl libnet-ssleay-perl libterm-readkey-perl

RUN useradd -ms /bin/bash user && mkdir -p /home/user/.local/bin
ENV PATH="/home/user/.local/bin:$PATH"
WORKDIR /home/user

RUN curl -L http://cpanmin.us | perl - App::cpanminus
RUN cpanm --notest --quiet Dist::Zilla
RUN CACHE=0.0413 git clone https://gitlab.com/cpran/CPrAN.git /home/user/cpran
RUN cpanm --notest --quiet https://gitlab.com/cpran/CPrAN.git

RUN CACHE=6.0.29 cpran --yes install \
  --path=/usr/bin --barren praat && \
  chmod 755 /usr/bin/praat

RUN chown -R user:user /home/user
USER 1000

RUN cpran --version && cpran init --notest
